﻿namespace CSS.Util
{
	/// <summary>
	/// Separates TraderKindDefs in different categories.
	/// </summary>
	public enum TraderKindCategory
	{
		Orbital,
		Settlement,
		Caravan,
		Visitor,
		None
	}
}