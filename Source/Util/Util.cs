﻿using System;
using System.Collections.Generic;
using RimWorld;
using Verse;

namespace CSS.Util
{
	/// <summary>
	/// Helper functions.
	/// </summary>
	public static class Util
	{
		/// <summary>
		/// Some mods include traders who do not follow the rules assumed by CSS. Their categories are hardcoded here.
		/// </summary>
		private static readonly Dictionary<string, TraderKindCategory> _hardcodedTraders =
			new Dictionary<string, TraderKindCategory>
			{
				{"Orbital_Rimatomics", TraderKindCategory.Orbital}
			};

		/// <summary>
		/// Find out the trader kind category of a trader.
		/// </summary>
		/// <param name="def">Trader being evaluate</param>
		/// <returns>Trader category.</returns>
		public static TraderKindCategory GetCategory(TraderKindDef def)
		{
			if (_hardcodedTraders.ContainsKey(def.defName))
			{
				return _hardcodedTraders[def.defName];
			}

			if (def.orbital)
			{
				return TraderKindCategory.Orbital;
			}

			foreach (var factionDef in DefDatabase<FactionDef>.AllDefs)
			{
				if (factionDef.baseTraderKinds.Contains(def)) return TraderKindCategory.Settlement;
				if (factionDef.caravanTraderKinds.Contains(def)) return TraderKindCategory.Caravan;
				if (factionDef.visitorTraderKinds.Contains(def)) return TraderKindCategory.Visitor;
			}

			return TraderKindCategory.None;
		}
	}
}