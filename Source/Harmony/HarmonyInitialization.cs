﻿using System.Reflection;
using Verse;

namespace CSS.Harmony
{
	/// <summary>
	/// Initialization of the Harmony patching of the mod.
	/// </summary>
	[StaticConstructorOnStartup]
	public class HarmonyInitialization
	{
		/// <summary>
		/// Initialization of the Harmony patching of the mod.
		/// </summary>
		static HarmonyInitialization()
		{
			// Initialize state.
			var harmony = new HarmonyLib.Harmony("joseasoler.CustomizableSilverStock");
			// Annotation patches.
			harmony.PatchAll(Assembly.GetExecutingAssembly());
		}
	}
}