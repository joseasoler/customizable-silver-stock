﻿using System;
using System.Collections.Generic;
using CSS.Mod;
using CSS.Util;
using HarmonyLib;
using RimWorld;
using Verse;

namespace CSS.Harmony
{
	[HarmonyPatch]
	public static class StockGeneratorCount
	{
		private static Dictionary<ushort, TraderKindCategory>
			_categoryByHash = new Dictionary<ushort, TraderKindCategory>();

		[HarmonyPostfix]
		[HarmonyPatch(typeof(StockGenerator), nameof(StockGenerator.RandomCountOf))]
		private static void ModifyGeneratedAmounts(ref StockGenerator __instance, ref int __result, ThingDef def)
		{
			if (def != ThingDefOf.Silver)
			{
				return;
			}

			if (!_categoryByHash.ContainsKey(def.shortHash))
			{
				_categoryByHash[__instance.trader.shortHash] = Util.Util.GetCategory(__instance.trader);
			}

			var category = _categoryByHash[__instance.trader.shortHash];
			if (category == TraderKindCategory.None)
			{
				var str = $"[CSS] Failed to find TraderKindCategory of trader {__instance.trader.defName}!";
				Log.ErrorOnce(str, str.GetHashCode());
				return;
			}

			__result = (int) (Settings.GetSilverScaling(category) * __result / 100.0f);
		}
	}
}