Customizable Silver Stock
===

[![RimWorld](https://img.shields.io/badge/RimWorld-1.4-informational)](https://rimworldgame.com/) [![Steam Downloads](https://img.shields.io/steam/downloads/2813426619)](https://steamcommunity.com/sharedfiles/filedetails/?id=2813426619) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)

[Customizable Silver Stock](https://steamcommunity.com/sharedfiles/filedetails/?id=2813426619) is a [RimWorld](https://rimworldgame.com/) mod which lets you change the amount of silver carried by traders.

There are separate settings for tweaking orbital traders, caravans, visitors and settlements, so you can customize each one to your liking. This feature is compatible with all traders from all mods!

Customizable Silver Stock can be added to existing games. You can also safely modify any of the silver amount settings at any point during the game. The changes will apply to any new traders that appear, or to settlements when they restock. Existing traders and settlements (until they restock) will remain unaffected.

Development
---

To compile this mod on Windows, you will need to install the [.NET Framework 4.7.2 Developer Pack](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net472). On Linux the packages you need vary depending on your distribution of choice. Dependencies are managed using NuGet. Compiling this project does not require any [external dependencies or extra setup steps](https://ludeon.com/forums/index.php?topic=49914.0).

Contributions
---

This project encourages community involvement and contributions. Check the [CONTRIBUTING](CONTRIBUTING.md) file for details. Existing contributors can be checked in the [contributors list](https://gitlab.com/joseasoler/customizable-silver-stock/-/graphs/main).

License
---

This project is licensed under the MIT license. Check the [LICENSE](LICENSE) file for details.

Acknowledgements
---

Read the [ACKNOWLEDGEMENTS](ACKNOWLEDGEMENTS.md) file for details.
